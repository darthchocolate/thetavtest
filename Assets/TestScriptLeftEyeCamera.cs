﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScriptLeftEyeCamera : MonoBehaviour
{
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        Debug.Log("Number of web cams connected: " + devices.Length);

        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(i + " " + devices[i].name);
        }

        Renderer rend = this.GetComponentInChildren<Renderer>();

        WebCamTexture myleftcam = new WebCamTexture();
        string camName = devices[2].name;
        Debug.Log("The webcam name is " + camName);
        myleftcam.deviceName = camName;
        rend.material.mainTexture = myleftcam;

        myleftcam.Play();
    }
}
