﻿using UnityEngine;
using System.Collections;

public class testScriptBasic : MonoBehaviour {

	void Start () {
		WebCamDevice[] devices = WebCamTexture.devices;
		Debug.Log("Number of web cams connected: " + devices.Length);

        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(i + " " + devices[i].name);
        }
            
        Renderer rend = this.GetComponentInChildren<Renderer>();

		WebCamTexture myrightcam = new WebCamTexture();
		string camName = devices[0  ].name;
		Debug.Log("The webcam name is " + camName);
		myrightcam.deviceName = camName;
		rend.material.mainTexture = myrightcam;

		myrightcam.Play();
	}    	
}